import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import os

class RedLinealQ(nn.Module):
    ##Red neuronal
    def __init__(self, size_entrada, size_oculto, size_salida):
        super().__init__()
        self.lineal1 = nn.Linear(size_entrada, size_oculto)
        self.lineal2 = nn.Linear(size_oculto, size_salida)

    def save(self, file_name='model.pth'):
        model_folder_path = './Model'

        if not os.path.exists(model_folder_path):
            #Si no existe crear
            os.makedirs(model_folder_path)

        file_name = os.path.join(model_folder_path, file_name)
        torch.save(self.state_dict(), file_name)


    def forward(self, x):
        x = F.relu(self.lineal1(x))
        x = self.lineal2(x)
        return x


class EntrenadorQL:
    def __init__(self, model, lr, gamma):
        self.lr = lr
        #mean squared error
        #(Qnuevo-Q)^2
        self.criterion = nn.MSELoss()

        self.model = model
        self.gamma = gamma
        self.optimizer = optim.Adam(model.parameters(), lr=self.lr)
        

    def train_step(self, state, action, reward, next_state, done):
        state = torch.tensor(state, dtype=torch.float)
        next_state = torch.tensor(next_state, dtype=torch.float)
        action = torch.tensor(action, dtype=torch.long)
        reward = torch.tensor(reward, dtype=torch.float)
        # (n, x)

        if len(state.shape) == 1:
            # (1, x)
            state = torch.unsqueeze(state, 0)
            next_state = torch.unsqueeze(next_state, 0)
            action = torch.unsqueeze(action, 0)
            reward = torch.unsqueeze(reward, 0)
            done = (done, )

        # Predecir los valores Q con el estado actual
        pred = self.model(state)

        target = pred.clone()
        for i in range(len(done)):
            Q_new = reward[i]
            if not done[i]:
                Q_new = reward[i] + self.gamma * torch.max(self.model(next_state[i]))

            target[i][torch.argmax(action[i]).item()] = Q_new
    
        # Q_new = r + y * max(next_predicted Q value) -> only do this if not done
        
        self.optimizer.zero_grad() #####empty gradient
        loss = self.criterion(target, pred)
        loss.backward()
        self.optimizer.step()
