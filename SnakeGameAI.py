import pygame
import random
from enum import Enum
from collections import namedtuple
import numpy as np



pygame.init()
font = pygame.font.SysFont('arial', 25)

class Direction(Enum):
    RIGHT = 1
    LEFT = 2
    UP = 3
    DOWN = 4


Point = namedtuple('Point', 'x, y')

# rgb colors
WHITE = (255, 255, 255)
RED = (200,0,0)
BLUE1 = (0, 0, 255)
BLUE2 = (0, 100, 255)
BLACK = (0,0,0)
PINK = (255,0,128)
VERDE_PASTO=(60,179,113)
YELLOW =(255,165,0)
BLOCK_SIZE = 20


## Se cambia el speed a mayor numero, para que el entrenamiento sea
#mas rapido.
SPEED = 40


class SnakeGameAI:
    def __init__(self, w=640, h=480):


        self.w = w

        self.h = h

        # Inicializar la visualizacion

        self.display = pygame.display.set_mode((self.w, self.h))
        pygame.display.set_caption('Snake')

        self.clock = pygame.time.Clock()
        self.reset()



    # Reiniciar todo el juego 
    def reset(self):

        # Inicializar el juego 
        self.direction = Direction.RIGHT
        self.head = Point(self.w/2, self.h/2)
        self.snake = [self.head,
                      Point(self.head.x-BLOCK_SIZE, self.head.y),
                      Point(self.head.x-(2*BLOCK_SIZE), self.head.y)]

        self.score = 0
        self.food = None
        self._place_food()
        self.num_iteracion = 0


    def _place_food(self):

        x = random.randint(0, (self.w-BLOCK_SIZE )//BLOCK_SIZE )*BLOCK_SIZE
        y = random.randint(0, (self.h-BLOCK_SIZE )//BLOCK_SIZE )*BLOCK_SIZE

        self.food = Point(x, y)
        if self.food in self.snake:
            self._place_food()

    def play_step(self, action):

        self.num_iteracion += 1

        #Recoleccion de lo que el AI decide hacer

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        
        #Moverse
        self._move(action) #Tendri que actualizarse la serpiente
        self.snake.insert(0, self.head)
        

        #Revisar si aun no perdió el juego
        premio = 0
        game_over = False

        #Pierde cuando colisina consigo mismo o con la pared
        #Tambien pierde cuando se queda en un ciclo infinito, "muere de hambre"
        if self.is_collision() or self.num_iteracion > 80*len(self.snake):
            game_over = True
            premio = -10
            return premio, game_over, self.score


        #Si no perdio, entonces        

        #Si consiguió la comida, aumenta score, y reposiciona la comida, y crece. 
        if self.head == self.food:
            self.score += 1
            premio = 10
            self._place_food()

        #Si aun no comio en tiempo, decrece.
        else:
            self.snake.pop()

        # actualizar ui y reloj
        self._update_ui()
        self.clock.tick(SPEED)

        # devuelve game over y score
        return premio, game_over, self.score

    def is_collision(self, pt=None):

        if pt is None:
            pt = self.head

        # LLega al limite

        if pt.x > self.w - BLOCK_SIZE or pt.x < 0 or pt.y > self.h - BLOCK_SIZE or pt.y < 0:

            return True

        # Se da a sí mismo

        if pt in self.snake[1:]:
            return True

        return False


    def _update_ui(self):
        self.display.fill(VERDE_PASTO)
        for pt in self.snake:
            pygame.draw.rect(self.display, YELLOW, pygame.Rect(pt.x, pt.y, BLOCK_SIZE, BLOCK_SIZE))
            pygame.draw.rect(self.display, BLUE2, pygame.Rect(pt.x+4, pt.y+4, 12, 12))

        pygame.draw.rect(self.display, PINK, pygame.Rect(self.food.x, self.food.y, BLOCK_SIZE, BLOCK_SIZE))

        text = font.render("Score: " + str(self.score), True, WHITE)
        self.display.blit(text, [0, 0])
        pygame.display.flip()




    def _move(self, action):


        # [recto, derecha, izquierda]
        clock_wise = [Direction.RIGHT, Direction.DOWN, Direction.LEFT, Direction.UP]
        idx = clock_wise.index(self.direction)

        if np.array_equal(action, [1, 0, 0]):
            new_dir = clock_wise[idx] # no change
        
        elif np.array_equal(action, [0, 1, 0]):
            next_idx = (idx + 1) % 4
            new_dir = clock_wise[next_idx] # derecha,abajo,izquierda,arriba

        else: # [0, 0, 1]
            next_idx = (idx - 1) % 4
            new_dir = clock_wise[next_idx] # derecha,arriba,izquierda, abajo



        self.direction = new_dir
        x = self.head.x
        y = self.head.y
        if self.direction == Direction.RIGHT:
            x += BLOCK_SIZE

        elif self.direction == Direction.LEFT:
            x -= BLOCK_SIZE
        elif self.direction == Direction.DOWN:
            y += BLOCK_SIZE
        elif self.direction == Direction.UP:
            y -= BLOCK_SIZE

        self.head = Point(x, y)